package com.gorest.app.pages;

import com.gorest.app.model.ApiModel;
import com.gorest.app.model.CreateUser;
import io.restassured.http.ContentType;
import io.restassured.response.ValidatableResponse;

import static net.serenitybdd.rest.SerenityRest.given;
import static org.hamcrest.CoreMatchers.is;

public class GoRestController {
    String baseUri = "https://gorest.co.in/public-api";
    String token = "f178f93bf783ad4c3d97b66c9fe2fe56c7df7cd827dfaba7ac26c4bf21ef647a";
    public ValidatableResponse  getUsers() {
        return given()
                .relaxedHTTPSValidation()
                .baseUri(baseUri)
                .basePath("/users")
                .log().all()
                .contentType(ContentType.JSON)
                .accept(ContentType.JSON)
                .when()
                .get("/")
                .then()
                .statusCode(200);
    }

    public CreateUser createUser(String body) {
        return given()
                .relaxedHTTPSValidation()
                .baseUri(baseUri)
                .basePath("/users")
                .log().all()
                .body(body)
                .contentType(ContentType.JSON)
                .accept(ContentType.JSON)
                .header("Authorization", "Bearer "+ token)
                .when()
                .post("/")
                .then()
                .statusCode(200)
                .body("code", is(201))
                .extract()
                .as(CreateUser.class);
    }

    public ApiModel deleteUser(int id) {
        return given()
                .relaxedHTTPSValidation()
                .baseUri(baseUri)
                .basePath("/users")
                .log().all()
                .contentType(ContentType.JSON)
                .accept(ContentType.JSON)
                .header("Authorization", "Bearer "+ token)
                .when()
                .delete(id+"")
                .then()
                .statusCode(200)
                .body("code", is(204))
                .extract()
                .as(ApiModel.class);
    }
}
