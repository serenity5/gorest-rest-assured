package com.gorest.steps;

import com.gorest.app.model.ApiModel;
import com.gorest.app.model.CreateUser;
import com.gorest.app.model.User;
import com.gorest.app.pages.GoRestController;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.restassured.module.jsv.JsonSchemaValidator;
import io.restassured.path.json.JsonPath;
import io.restassured.response.ValidatableResponse;
import net.thucydides.core.annotations.Steps;

import java.util.List;
import java.util.stream.Collectors;

import static java.util.Arrays.asList;
import static org.junit.Assert.*;

public class GoRestDefinitions {
    @Steps
    private GoRestController goRestController = new GoRestController();
    ApiModel apiResponse;
    ValidatableResponse validatableResponse;
    CreateUser createUserResponse;
    User user;
    JsonPath jsonPath;

    @Given("Get all users")
    public void getUsers() {
        getUsersRequest();
    }

    @Then("Check user {string} not exist")
    public void userShouldBeNotFound(String name) {
        List<User> users = apiResponse.getUsers().stream().filter(user -> user.getName()
                .equalsIgnoreCase(name)).collect(Collectors.toList());
        assertEquals(asList(), users);
    }

    @Given("Create user Demo Test")
    public void createUser(String user) {
        jsonPath = JsonPath.from(user);
        createUserResponse = goRestController.createUser(user);
    }

    @Then("Check created user exist")
    public void userShouldBeExist() {
        user = createUserResponse.getUser();
        assertEquals(jsonPath.get("email"), user.getEmail());
        assertEquals(jsonPath.get("name"), user.getName());
        assertEquals(jsonPath.get("gender"), user.getGender());
        assertEquals(jsonPath.get("status"), user.getStatus());
    }

    @Given("Delete created user")
    public void deleteCreatedUser() {
        apiResponse = goRestController.deleteUser(user.getId());
    }

    @Then("Check user deleted")
    public void userShouldBeDeleted() {
        assertEquals(Integer.valueOf(204), apiResponse.getCode());
    }

    @Given("Get all users after deleting user")
    public void getUsersAfterDeleting() {
        getUsersRequest();
    }

    @Then("Check created user no exist")
    public void userShouldBeNoExist() {
        List<User> users = apiResponse.getUsers().stream().filter(user -> user.getId()
                .equals(this.user.getId())).collect(Collectors.toList());
        assertEquals(asList(), users);
    }

    @Then("Check users json scheme")
    public void jsonSchemeShouldBe() {
        validatableResponse.assertThat().body(JsonSchemaValidator.matchesJsonSchemaInClasspath("scheme/apiResponseScheme.json"));
    }

    public void getUsersRequest(){
        validatableResponse = goRestController.getUsers();
        apiResponse = validatableResponse.extract().as(ApiModel.class);
    }
}
