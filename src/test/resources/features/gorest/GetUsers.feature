Feature: API Automation for GoRest

  @Task1
  Scenario: Check user creating and deleting
    Given Get all users
    Then Check user "Demo Test" not exist
    Given Create user Demo Test
      """
      {
       "name":"Demo Test",
       "gender":"Male",
       "email":"demo.test14@test.com",
       "status":"Active"
    }
      """
    Then Check created user exist
    Given Delete created user
    Then Check user deleted
    Given Get all users after deleting user
    Then Check users json scheme